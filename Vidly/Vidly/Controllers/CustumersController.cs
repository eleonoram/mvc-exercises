﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers
{
    public class CustumersController : Controller
        
    {
        private ApplicationDbContext _context;
        public CustumersController()
        {
            _context = new ApplicationDbContext();

        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }



        // GET: Custumers
        public ViewResult Index()
        {
            var custumers = _context.Custumers.Include(c => c.MembershipType).ToList();

            return View(custumers);

        }
        public ActionResult Details(int id)

        {

            var custumer = _context.Custumers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);



            if (custumer == null)

                return HttpNotFound();



            return View(custumer);

        }

      
    }
}