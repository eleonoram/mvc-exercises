namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplyAnnotationsToCostumerName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Custumers", "Name", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Custumers", "Name", c => c.String());
        }
    }
}
