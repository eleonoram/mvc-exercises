namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBirthdayToCostumerClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Custumers", "BirthDate", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Custumers", "BirthDate");
        }
    }
}
