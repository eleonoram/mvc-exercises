namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBirthdayToCostumerClass1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Custumers", "BirthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Custumers", "BirthDate", c => c.Int(nullable: false));
        }
    }
}
